#!/usr/bin/env python
#coding:utf-8

"""
Question:
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that
the 6th prime is 13.

What is the 10 001st prime number?
"""
from ex5 import prime

primelist = []
i = 0
tmp = prime(1)
while len(primelist) <= 10000:
    if tmp.primejudge(i):
        primelist.append(i)
    i += 1

print primelist[-1]
