#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Question:
 A Pythagorean triplet is a set of three natural numbers,
 a < b < c, for which,
    a^2 + b^2 = c^2

For example: 3^2 + 4^2 = 9 + 16 = 25
There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product a*b*c.
"""

def Pythagorean(numsum):
    """
    Pythagorean.doc
    """
    for a in xrange(1, numsum/3):
        for b in xrange(1, numsum/2):
            c = numsum - a -b
            if (a**2 +b**2) == c**2:
                return (a, b, c)
    return (None, None, None)
    
if __name__ == "__main__":
    a, b, c = Pythagorean(1000)
    print a, b, c
    print a*b*c 
