#!/usr/bin/env python
#coding:utf-8

"""
Question:
The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?
"""
def primejudge(number):
    if number == 1:
        return False
    if number == 2:
        return True
    if number % 2 == 0:
        return False
    for i in xrange(2,(number+1)/2):
        if number % i == 0:
            return False
    return True

target = 600851475143
#target = 13195
y = 2
try:
    print "The prime factor has:",
#    for y in range(1, (target/2)+1):
    while y <= target/2:
        if target % y == 0 and primejudge(y):
            print y,
            target = target / y
        
        y += 1
    if primejudge(target):
        print target

except OverflowError,e:
    print "\ntarget too long, range functinon cannot cover"
