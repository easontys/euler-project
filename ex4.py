#!/usr/bin/env python
#coding:utf-8

"""
Question:
A palindromic number reads the same both ways. The largest palindrome made from
the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.
"""

def palindromicjudge(number):
    number_list = list(str(number))
    if list(reversed(number_list)) == number_list:
        return True
    return False

palindromic_list = list(reversed([number for number in xrange(100*100, 1000*1000)\
                    if palindromicjudge(number)]))

print "All 3-digit number can product",len(palindromic_list),
print "palindromic number"


for number in palindromic_list:
    for i in xrange(1000, 100, -1):
        if number % i == 0:
            y = number / i
            if len(str(y)) >3:
                continue
            else:
                print "\nGet Two Number:",i,y,
                print "\t\t",i*y
                exit()
