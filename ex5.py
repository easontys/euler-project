#!/usr/bin/env python
#coding:utf-8

"""
Question:
2520 is the smallest number that can be divided by each of the numbers from 1
to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the
numbers from 1 to 20?
"""
def positivenumber(number, limit):
    for i in xrange(1,  limit+1):
        if number % i != 0:
            return False
    return True

#print positivenumber(2520, 10)

class prime():
    def __init__(self, number):
        self.number = number

    def primejudge(self, number):
        if number == 1:
            return False
        elif number == 2:
            return True
        if number % 2 == 0:
            return False
        for i in range(2, (number+1)/2):
            if number % i == 0:
                return False
        return True

    def primelist(self):
        try:
            return [i for i in xrange(1, self.number+1) if self.primejudge(i)]
        except OverflowError, e:
            return "Too long number, cannot return primelist"

    def primefactor(self):
        primefactorlist = []
        num = self.number
        factortmp = 2
        while factortmp <= num / 2:
            if num % factortmp == 0 and self.primejudge(factortmp):
                primefactorlist.append(factortmp)
                num /= factortmp
            factortmp += 1
        primefactorlist.append(int(num))

        return sorted(set(primefactorlist))

if __name__ == "__main__":
    def maxer(number):
        tmp = number
        while tmp*number <= 20:
            number *= tmp
        return number

    target = 1
    for i in prime(20).primelist():
        target *= maxer(i)
    
    print target     

